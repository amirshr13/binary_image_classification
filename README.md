## Simple CNN For Binary Image Classification
This project initialy created for classify santa and not_santa images but you can
use it for other purposes.


## Prerequisites
For training the network you'll need you'r own datasets,
by ruuning the train_network.py file and specify the -d --dataset argument
train the network on you'r datasets.

-m --model argument is the path to output model.

-p --plot argument is the path to output accuracy/loss plot.

## Deployment
After training the model you can run prediction.py file and specify the name of 
the image by -i --image argument and model path by -m --model argument,
the results will be shown on the image.



