from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import cv2


class Prediction:
    def __init__(self):
        self.args = self.set_argparse()
        self.img, self.copy_img = self.preprocess()

    @staticmethod
    def set_argparse():
        ap = argparse.ArgumentParser()
        ap.add_argument("-m", "--model", required=True,
                        help="path to trained model model")
        ap.add_argument("-i", "--image", required=True,
                        help="path to input image")
        return vars(ap.parse_args())

    def preprocess(self):
        img = cv2.imread(self.args['image'])
        copy_img = img.copy()
        img = cv2.resize(img, (128, 128))
        img = img.astype('float') / 255.0
        img = img_to_array(img)
        img = np.expand_dims(img, axis=0)
        return img, copy_img

    def load_model(self):
        print('[INFO] loading network...')

        model = load_model(self.args['model'])
        not_santa, santa = model.predict(self.img)[0]

        label = "Santa" if santa > not_santa else "Not Santa"
        proba = santa if santa > not_santa else not_santa
        label = "{}: {:.2f}%".format(label, proba * 100)

        output = imutils.resize(self.copy_img, width=400)
        cv2.putText(output, label, (10, 25), cv2.FONT_HERSHEY_SIMPLEX,
                    0.7, (0, 255, 0), 2)

        cv2.imshow("Output", output)
        cv2.waitKey(0)


if __name__ == '__main__':
    pred = Prediction()
    pred.set_argparse()
    pred.preprocess()
    pred.load_model()
