from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
from model import Model
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import random
import cv2
import os



class TrainNetwork(Model):
    def __init__(self, epochs=40, init_learning_rate=1e-3, batch_size=40):
        self.args = self.set_argparse()
        self.epochs = epochs
        self.init_learning_rate = init_learning_rate
        self.batch_size = batch_size
        self.data, self.labels = self.preprocess()
        self.train_x, self.test_x, self.train_y, self.test_y = self.splitting()

    @staticmethod
    def set_argparse():
        ap = argparse.ArgumentParser()
        ap.add_argument('-d', '--dataset', required=True, help='path to input dataset')
        ap.add_argument('-m', '--model', required=False, help='path to output model',
                        default='santa_classification.dat')
        ap.add_argument('-p', '--plot', type=str, default='plot.png',
                        help='path to output accuracy/loss plot')
        args = vars(ap.parse_args())
        return args

    def preprocess(self):
        images_paths = sorted(list(paths.list_images(self.args['dataset'])))
        random.shuffle(images_paths)

        data = []
        labels = []

        print('[INFo] loading images...')

        for img_path in images_paths:
            image = cv2.imread(img_path)
            image = cv2.resize(image, (128, 128))
            image = img_to_array(image)
            data.append(image)
            label = img_path.split(os.path.sep)[-2]
            label = 1 if label == 'santa' else 0
            labels.append(label)
        return data, labels

    def splitting(self):
        data = np.array(self.data, dtype="float") / 255.0
        labels = np.array(self.labels)

        train_x, test_x, train_y, test_y = train_test_split(data, labels, test_size=.25)

        train_y = to_categorical(train_y, num_classes=2)
        test_y = to_categorical(test_y, num_classes=2)

        return train_x, test_x, train_y, test_y

    def train_model(self, ):
        aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1,
                                 height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
                                 horizontal_flip=True, fill_mode="nearest")

        print('[INFO] compiling model...')

        model = Model.build(width=128, height=128, depth=3, classes=2)
        opt = Adam(lr=self.init_learning_rate, decay=self.init_learning_rate / self.epochs)
        model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
        print('[INFO] training network...')
        model_plot = model.fit_generator(aug.flow(self.train_x, self.train_y, batch_size=self.batch_size),
                                         validation_data=(self.test_x, self.test_y),
                                         steps_per_epoch=len(self.train_x) // self.batch_size,
                                         epochs=self.epochs, verbose=1)
        model.save(self.args['model'])

        plt.style.use("ggplot")
        plt.figure()
        plt.plot(np.arange(0, self.epochs), model_plot.history["loss"], label="train_loss")
        plt.plot(np.arange(0, self.epochs), model_plot.history["val_loss"], label="val_loss")
        plt.plot(np.arange(0, self.epochs), model_plot.history["acc"], label="train_acc")
        plt.plot(np.arange(0, self.epochs), model_plot.history["val_acc"], label="val_acc")
        plt.title("Training Loss and Accuracy on Santa/Not Santa")
        plt.xlabel("Epochs")
        plt.ylabel("Loss/Accuracy")
        plt.legend(loc="lower left")
        plt.savefig(self.args["plot"])
        plt.show()


if __name__ == '__main__':
    network = TrainNetwork()
    network.train_model()
